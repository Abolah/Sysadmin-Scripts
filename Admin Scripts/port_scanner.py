import sys
import socket
from datetime import datetime
import threading

threads = []
time1 = datetime.now()
host = "198.46.141.66" # hostname or IP
getport = "1-65535".split("-") #if you want to scan a specific port, remove the split otherwise don't touch this param

# Just a banner :P
print('=' * 65)
print("Scan started against {}".format(host) + " at " + str(datetime.now()))
print('=' * 65)


def scan(host, ports):
    socket.setdefaulttimeout(1)
    result = socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect_ex((host, ports))
    if result == 0:
        print("Discovered open port {}/tcp".format(ports))

        socket.socket(socket.AF_INET, socket.SOCK_STREAM).close()

for ports in range(int(getport[0]), int(getport[1])):
    t1 = threading.Thread(target=scan, args=(host, ports))
    threads.append(t1)
    t1.start()

for thread in threads:
    thread.join()

time2 = datetime.now()
total = time2 - time1
print("=" * 45)
print("Scanning completed in: ", total)
print("=" * 45)
